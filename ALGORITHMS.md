# Algorithms

MLASEngine contains a growing number of algorithms to add to a calculation
model and automatically add input like the position of the search grids
of the phreatic line. This documentation section explains the different 
available algorithms and their expected settings.

## Loads and waterlevels

### Phreatic line

To add a phreatic line use the AlgorithmSettingsAddPhreaticLine settings class and add it to function
to create a stix file. Here is some example code:

```python
# define parameters for the phreatic line
pl_settings = AlgorithmSettingsAddPhreaticLine(
    waterlevel = 0.0,
    waterlevel_polder = -2.5,
    offset_points = [Point2D(x=3, z=-1.0)]
)

# call the final function with the phreatic line settings   
stix_from_soilinvestigation(
    owner='waternet',
    ...
    pl_settings=pl_settings, 
    ...
)
```

This will result in an added phreatic line. Note that the algorithm might make funny decisions if the input is funny so always check what is happening!

![plline](https://gitlab.com/breinbaas/mlas/-/raw/master/img/plline.png)

#### Settings

The phreatic line algorithms accepts the following settings;

* waterlevel, the waterlevel on the levee side
* waterlevel_polder, the waterlevel on the polder side
* waterlevel_offset, the minimum distance between the ground and the phreatic line
* slope, the slope of the line 
* offset_points, user defined point to offset from the reference line

![plline](https://gitlab.com/breinbaas/mlas/-/raw/master/img/pllinesettings.png)

**Note** for the algorithms to work properly you will need to have the ditch points defined in the crosssection. 
This will automatically be added if you used ditch data raster files for your crosssection creation. 


### Traffic load

To add a traffic load to the calculation file use the AlgorithmSettingsTrafficLoad settings class and add it to function
to create a stix file. Here is some example code:

```python
# define parameters for the trafficload
tl_settings = AlgorithmSettingsTrafficLoad(
    name = "verkeer",
    offset = -3.0,
    magnitude = 13.0,
    width = 2.5,
    distribution_angle = 45.0        
)

# call the final function with the phreatic line settings   
stix_from_soilinvestigation(
    owner='waternet',
    ...
    tl_settings = tl_settings, 
    ...
)
```

This will add the trafficload at the assigned / calculated location.

![tl settings](https://gitlab.com/breinbaas/mlas/-/raw/master/img/trafficload.png)

#### Settings

The traffic load algorithm accepts the following settings;

* name, name of the load, defaults to ""
* reference_point_type, point_type to serve as reference to the rightmost point of the traffic load, defaults to crest right top
* offset, the distance from the previous point to the right (or left if negative)
* magnitude, magnitude of the load, defaults to 0.0
* width, width of the load, defaults to 0.0
* distribution_angle, distribution angle of the load in degrees, defaults to 0.0
* consolidation_coefficient, consolidation coefficient, defaults to 0.0

## Analysis methods

### Bishop Brute Force

To add the definition of the search area and tangent area of the calculation use the AlgorithmSettingsDStabilityBishopBruteForce settings class and add it to function
to create a stix file. Here is some example code:

```python
# define parameters for the searchgrid and tangent lines
bbf_settings = AlgorithmSettingsDStabilityBishopBruteForce(
    searcharea_min_width = 10, 
    searcharea_min_height = 10,
    searcharea_resolution = 1,
    tangentarea_zmin = -8,
    tangentarea_min_height = 5,
    tangentarea_resolution = 1
)

# call the final function with the phreatic line settings   
stix_from_soilinvestigation(
    owner='waternet',
    ...
    bbf_settings = bbf_settings, 
    ...
)
```

This will result in an added search parameters. Note that the algorithm might make funny decisions if the input is funny so always check what is happening!

![bbf](https://gitlab.com/breinbaas/mlas/-/raw/master/img/bishopbruteforce.png)

#### Settings

The bishop brute force algorithms accepts the following settings;

* searcharea_min_width, the minimum width of the search area
* searcharea_min_height, the minimum height of the search area
* searcharea_resolution, the distance between the points in the search area
* tangentarea_zmin, the lowest point of the tangent lines
* tangentarea_min_height, the minimum height of the tangent lines search area
* tangentarea_resolution, the distance between two tangent lines
* minimum_slip_plane_depth: float = 0.0, only find slip planes with a depth (from the toplayer) if the given value, defaults to 0.0
* minimum_slip_plane_length: float = 2.0, only find slip planes with the given length or longer, defaults to 2.0


![bbf settings](https://gitlab.com/breinbaas/mlas/-/raw/master/img/bishopbruteforcesettings.png)

**Note** I am working on a way to automatically assign the tangentarea_zmin so keep posted.

### Spencer Genetic Algorithm

To add the definition of the Spencer genetic algorithm use the AlgorithmSettingsDStabilitySpencerGenetic settings class and add it to function
to create a stix file. Here is some example code:

```python  
sga_settings = AlgorithmSettingsDStabilitySpencerGenetic()  # yep, no parameters necessary

# call the final function with the phreatic line settings   
stix_from_soilinvestigation(
    owner='waternet',
    ...
    sga_settings = sga_settings, 
    ...
)
```

This will result in an added search parameters. Note that the algorithm might make funny decisions if the input is funny so always check what is happening!

![spencer genetic](https://gitlab.com/breinbaas/mlas/-/raw/master/img/spencergenetic.png)

#### Settings

The spencer genetic algorithms accepts the following settings;

* minimum_angle_between_slices: TODO, defaults to 0.0
* minimum_thrust_line_percentage_inside_slices: TODO, defaults to 0.0
* depth_in_slope: TODO, defaults to 1.0

