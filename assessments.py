from database import Database
from subprocess import run
from pydantic import BaseModel
from pathlib import Path
from typing import List, Any
from enum import IntEnum
import math
from tqdm import tqdm

from mlasengine.objects.cpt import CPT
from mlasengine.objects.borehole import Borehole
from mlasengine.objects.crosssection import Crosssection
from mlasengine.algorithms.add_phreatic_line import AlgorithmSettingsAddPhreaticLine
from mlasengine.algorithms.add_bishop_brute_force import AlgorithmSettingsDStabilityBishopBruteForce
from mlasengine.algorithms.add_spencer_genetic import AlgorithmSettingsDStabilitySpencerGenetic
from mlasengine.algorithms.add_trafficload import AlgorithmSettingsTrafficLoad

from generate import stix_from_soilinvestigation
from settings import PATH_DGEOSTAB_CONSOLE

def calc_stix(filename: str):
    filepath = Path(filename).resolve().parent
    process = run(
        [str(PATH_DGEOSTAB_CONSOLE)] + [filename],
        timeout=10,
        #cwd=str(filename),
    )

class SoilInvestigationLocation(IntEnum):
    CREST = 0
    POLDER = 1

class SettingsArea(BaseModel):
    chainage_start: int
    chainage_end: int
    settings: Any


class AssessmentLeveeStabilityInward(BaseModel):
    #required
    owner: str
    fillmaterial_top: str  
    crosssections: List[Crosssection]

    # optional (well you will need at least one cpt or borehole)
    cpts_crest: List[CPT] = [],
    cpts_polder: List[CPT] = []
    boreholes_crest: List[Borehole] = []
    boreholes_polder: List[Borehole] = []    
    
    # calculation settings
    pl_setting_areas: List[SettingsArea] = [] # phreatic line settings
    tl_setting_areas: List[SettingsArea] = [] # traffic load settings
    bbf_setting_areas: List[SettingsArea] = [] # bishop brute force settings
    sga_setting_areas: List[SettingsArea] = [] # spencer generative algorithm settings
    
    # export to
    output_path: str = ""

    def _closest_cpt(self, x: float, y: float, si_location=SoilInvestigationLocation.CREST, max_distance: float=1e9) -> CPT:
        """
        Get CPT closest to given location

        Args:
            x (float): x coordinate
            y (float): y coordinate            
            si_location (SoilInvestigationLocation): location of the soilinvestigation, defaults to SoilInvestigationLocation.CREST
            max_distance (float): maximum distance to the CPT, defaults to 1e9m

        Returns:
            CPT closest to given location or None
        """
        result, dlmin = None, 1e9

        cpts = self.cpts_crest
        if si_location == SoilInvestigationLocation.POLDER:
            cpts = self.cpts_polder

        
        for cpt in cpts:
            dx = cpt.x - x
            dy = cpt.y - y
            dl = math.hypot(dx, dy)
            if dl < dlmin:
                result = cpt
                dlmin = dl

        return result

    # def _closest_borehole(self, x: float, y: float, si_location=SoilInvestigationLocation.CREST, max_distance: float=1e9, ) -> CPT:
    #     """
    #     Get Borehole closest to given location

    #     Args:
    #         x (float): x coordinate
    #         y (float): y coordinate
    #         si_location (SoilInvestigationLocation): location of the soilinvestigation, defaults to SoilInvestigationLocation.CREST
    #         max_distance (float): maximum distance to the Borehole, defaults to 1e9m

    #     Returns:
    #         CPT closest to given location or None
    #     """
    #     result, dlmin = None, 1e9

    #     boreholes = self.boreholes_crest
    #     if si_location == SoilInvestigationLocation.POLDER:
    #         boreholes = self.boreholes_polder

        
    #     for borehole in boreholes:
    #         dx = borehole.x - x
    #         dy = borehole.y - y
    #         dl = math.hypot(dx, dy)
    #         if dl < dlmin:
    #             result = borehole
    #             dlmin = dl

    #     return result

    

    def execute(self):
        for crs in tqdm(self.crosssections):
            cpt_crest = self._closest_cpt(crs.reference_point.x, crs.reference_point.y, SoilInvestigationLocation.CREST)
            cpt_polder = self._closest_cpt(crs.reference_point.x, crs.reference_point.y,SoilInvestigationLocation.POLDER)
            #borehole_crest = self._closest_borehole(crs.reference_point.x, crs.reference_point.y, SoilInvestigationLocation.CREST)
            #borehole_polder = self._closest_borehole(crs.reference_point.x, crs.reference_point.y, SoilInvestigationLocation.POLDER)

            pl_settings = None 
            for area in self.pl_setting_areas:
                if area.chainage_start <= crs.levee_chainage and crs.levee_chainage <= area.chainage_end:
                    pl_settings = area.settings

            tl_settings = None
            for area in self.tl_setting_areas:
                if area.chainage_start <= crs.levee_chainage and crs.levee_chainage <= area.chainage_end:
                    tl_settings = area.settings

            bbf_settings = None            
            for area in self.bbf_setting_areas:
                if area.chainage_start <= crs.levee_chainage and crs.levee_chainage <= area.chainage_end:
                    bbf_settings = area.settings

            sga_settings = None
            for area in self.sga_setting_areas:
                if area.chainage_start <= crs.levee_chainage and crs.levee_chainage <= area.chainage_end:
                    sga_settings = area.settings
            
            try:
                # bbf 
                if bbf_settings:
                    stix_from_soilinvestigation(
                        owner = self.owner,
                        crosssection = crs, 
                        object_left = cpt_crest, 
                        object_right = cpt_polder,
                        fillmaterial_top = self.fillmaterial_top,
                        pl_settings = pl_settings,
                        tl_settings = tl_settings,
                        bbf_settings = bbf_settings,
                        output_path = self.output_path 
                    )
                # sga
                if sga_settings:
                    stix_from_soilinvestigation(
                        owner = self.owner,
                        crosssection = crs, 
                        object_left = cpt_crest, 
                        object_right = cpt_polder,
                        fillmaterial_top = self.fillmaterial_top,
                        pl_settings = pl_settings,
                        tl_settings = tl_settings,
                        sga_settings = sga_settings,
                        output_path = self.output_path 
                    )

            except Exception as e:
                print(f"Could not create crosssection {crs.levee_chainage} with error '{e}'")


if __name__=="__main__":
    db = Database()
    crosssections = db.get_crosssections_from_chainages('waternet', 'A117', [2080, 2120, 2240, 2450])    

    cpts_crest, cpts_polder = [], []#boreholes_crest, boreholes_polder = [], [], [], []
    
    # IDs using QGis
    cpts_crest.append(db.get_cpt_by_id(15479))
    cpts_crest.append(db.get_cpt_by_id(15481))
    cpts_crest.append(db.get_cpt_by_id(15482))
    cpts_polder.append(db.get_cpt_by_id(15480))
    cpts_polder.append(db.get_cpt_by_id(15456))
    cpts_polder.append(db.get_cpt_by_id(15483))

    pl_setting_areas = [SettingsArea(
        chainage_start= 2020,
        chainage_end = 2500,
        settings = AlgorithmSettingsAddPhreaticLine(
            waterlevel = 0.0,
            waterlevel_polder = -5.0
        )
    )]

    tl_setting_areas = [SettingsArea(
        chainage_start= 2020,
        chainage_end = 2500,
        settings = AlgorithmSettingsTrafficLoad(
            name = "verkeer",
            offset = -3.0,
            magnitude = 13.0,
            width = 2.5,
            distribution_angle = 45.0         
        )    
    )]

    bbf_setting_areas = [SettingsArea(
        chainage_start= 2020,
        chainage_end = 2500,
        settings = AlgorithmSettingsDStabilityBishopBruteForce(
            searcharea_min_width = 10, 
            searcharea_min_height = 10,
            searcharea_resolution = 1,
            tangentarea_zmin = -8,
            tangentarea_min_height = 5,
            tangentarea_resolution = 1    
        )
    )]

    sga_setting_areas = [SettingsArea(
        chainage_start= 2020,
        chainage_end = 2500,
        settings = AlgorithmSettingsDStabilitySpencerGenetic()    
    )]

    stbi = AssessmentLeveeStabilityInward(
        owner = 'waternet',
        cpts_crest = cpts_crest,                # selection of crest cpts to use
        cpts_polder = cpts_polder,              # selection of polder cpts to use
        crosssections = crosssections,          # selection of crosssections to calculate
        fillmaterial_top = 'clay',              # top fill material 
        pl_setting_areas = pl_setting_areas,    # settings for phreatic line
        tl_setting_areas = tl_setting_areas,    # settings for traffic load
        bbf_setting_areas = bbf_setting_areas,  # settings for bishop brute force
        sga_setting_areas = sga_setting_areas,  # settings for spencer genetic algorithm
        output_path = "D:\_TEMP"                # path for the calculation input
    )

    stbi.execute()




    


