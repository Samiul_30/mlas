import glob, os
from pathlib import Path
from enum import IntEnum
import rasterio as rio
import numpy as np

from mlasengine.helpers import case_insensitive_glob

from settings import OWNERS, DIR_RASTERDATA, PATH_MLAS, DIR_DITCHDATA, DIR_RAW_HEIGHTDATA, DIR_HEIGHTDATA, DIR_WATERBOTTOM

INI_NAME = "tiles.ini"
ZLIMIT = 100000

class TilesetType(IntEnum):
    WATERBOTTOM = 0
    DITCHES = 1    
    HEIGHTDATA = 2
    RAWHEIGHTDATA = 10

class Tile:
    @classmethod
    def from_tif(self, filename):
        tile = Tile()

        r = rio.open(filename)
        tile.data = r.read(1, masked=True).data
        tile.boundary = {"left":r.bounds.left, "right":r.bounds.right, "bottom":r.bounds.bottom, "top":r.bounds.top}
        tile.shape = {"columns":r.meta['width'], "rows":r.meta['height']}
        tile.data = r.read(1, masked=True).data
        tile.resolution = {"x":r.res[1], "y":r.res[0]}
        tile.nodata = r.meta['nodata']
        tile.filename = filename
        return tile

    def __init__(self):
        self.boundary = {}
        self.resolution = {}
        self.shape = {}
        self.data = None
        self.nodata = None
        self.filename = ""

    def contains_point(self, x, y):
        return x <= self.boundary['right'] and x >= self.boundary['left'] and y>= self.boundary['bottom'] and y<=self.boundary['top']
    
    def _read(self):
        if self.data is None:
            #print("[I] reading {}".format(self.filename))
            r = rio.open(self.filename)
            self.data = r.read(1, masked=True).data

    def info(self):
        print("Boundaries  : ", self.boundary)
        print("Resolutie   : ", self.resolution)
        print("Data grootte: ", self.shape)
        print("No data     : ", self.nodata)

    def get_z(self, x, y):
        self._read()
        dx = x - self.boundary['left']
        dy = self.boundary['top'] - y

        idx = int(round(dx / self.resolution['x']))
        if idx<0 or idx>=self.shape['columns']:
            #print("X coordinaat {} valt niet in het bereik van deze data [{}, {}>".format(x, self.boundary['left'] - 0.5*self.resolution['x'], self.boundary['right']-0.5*self.resolution['x']))
            return np.nan

        idy = int(round(dy / self.resolution['y']))
        if idy<0 or idy>=self.shape['rows']:
            #rint("Y coordinaat {} valt niet in het bereik van deze data <{}, {}]".format(y, self.boundary['bottom'] + 0.5*self.resolution['y'], self.boundary['top'] + 0.5*self.resolution['y']))
            return np.nan

        z = self.data[idy,idx]
        if z > ZLIMIT or z < -ZLIMIT:
            return np.nan
        return z

class Tileset:
    def __init__(self, owner, tileset_type):
        self._owner = owner
        self._tiles = []
        self._type = tileset_type
        self._tilesdir = None
        self._inifile = None

        if self._type == TilesetType.DITCHES:
            self._tilesdir = Path(PATH_MLAS) / owner / DIR_RASTERDATA / DIR_DITCHDATA
        elif self._type == TilesetType.WATERBOTTOM:
            self._tilesdir = Path(PATH_MLAS) / owner / DIR_RASTERDATA / DIR_WATERBOTTOM
        elif self._type == TilesetType.RAWHEIGHTDATA:
            self._tilesdir = Path(PATH_MLAS) / owner / DIR_RASTERDATA / DIR_RAW_HEIGHTDATA
        
        self._inifile = os.path.join(self._tilesdir, INI_NAME)
        self._initialize_available_data()

    def get_tile_by_xy(self, x, y):
        for tile in self._tiles:
            if tile.contains_point(x, y):
                return tile
        
        return None        

    def setup(self):
        print(self._tilesdir)
        print("[I] Setting up ini file")
        fout = open(self._inifile, 'w')

        files = case_insensitive_glob(self._tilesdir, '.tif')

        if len(files)==0:
            print("No tif files found, checking for img files")
            files = case_insensitive_glob(self._tilesdir, '.img')

        i, itot = 1, len(files)
        fout.write('file;left;right;bottom;top;resolution_x;resolution_y;rows;columns;no_data\n')
        for file in files:
            print("[I] Handling file {} {}/{}".format(file, i, itot))
            r = rio.open(file)
            fout.write('{};{};{};{};{};{};{};{};{};{}\n'.format(file, r.bounds.left, r.bounds.right, r.bounds.bottom, r.bounds.top,
                                                 r.res[1], r.res[0], r.meta['width'], r.meta['height'], r.meta['nodata']))
            i+=1
        print("[I] Ini bestand is afgerond en opgeslagen.")
        fout.close()

    def _initialize_available_data(self):
        if os.path.isfile(self._inifile):
            self._read_ini()
        else:
            self.setup()
            self._read_ini()

    def _check_ini(self):
        result = True
        fin = open(self._inifile)
        lines = fin.readlines()
        fin.close()
        for line in lines[1:]:
            args = [s.strip() for s in line.split(';')]
            if not os.path.isfile(args[0]):
                print("[E] Het bestand {} is niet beschikbaar.".format(args[0]))
                result = False
        return result

    def _read_ini(self):
        if not self._check_ini():
            print("[E] Fout in ini bestand gevonden. Verwijder het bestand en run de setup opnieuw.")
            return

        fin = open(self._inifile)
        lines = fin.readlines()
        fin.close()
        for line in lines[1:]:
            args = [s.strip() for s in line.split(';')]
            tile = Tile()
            tile.filename = args[0]
            tile.resolution['x'] = float(args[5])
            tile.resolution['y'] = float(args[6])
            tile.boundary['left'] = float(args[1]) - tile.resolution['x'] / 2.
            tile.boundary['right'] = float(args[2]) - tile.resolution['x'] / 2.
            tile.boundary['bottom'] = float(args[3]) + tile.resolution['y'] / 2.
            tile.boundary['top'] = float(args[4]) + tile.resolution['y'] / 2.
            tile.shape['columns'] = int(args[7])
            tile.shape['rows'] = int(args[8])
            tile.nodata = float(args[9])
            self._tiles.append(tile)

        #print("[I] Ini bestand is ok")

    def get_z(self, x, y):
        """Get the lowest z value in the tiles because sometimes they seem to be overlapping especially with the waterbottom"""
        result = 1e9
        for tile in self._tiles:
            if tile.boundary['left'] <= x <= tile.boundary['right'] and tile.boundary['bottom'] <= y <= tile.boundary['top']:
                z = tile.get_z(x, y) 
                if not np.isnan(z): #some img / tif files overlap, so only return a value if it is not None, else look further
                    if z < result:
                        result = z
        
        if result == 1e9:
            return np.nan
        else:
            return result

if __name__=="__main__":
    ts = Tileset(
        owner = "waternet",
        tileset_type=TilesetType.DITCHES
    )

    for x in range(122216, 122228):
        print(x,ts.get_z(x,483569))