from logging import debug
from pathlib import Path
from typing import List, Tuple, Union
import numpy as np
from numpy.core.numeric import cross
from sqlalchemy.sql.expression import update
from tqdm import tqdm
import math
from shapely.geometry import Polygon
import matplotlib as plt
import matplotlib.pyplot as pyplot
from matplotlib.patches import Polygon as MPolygon
import io
from mlasengine.algorithms.add_spencer_genetic import AlgorithmSettingsDStabilitySpencerGenetic

from mlasengine.objects.crosssection import Crosssection
from mlasengine.objects.points import PointType, Point3D
from mlasengine.objects.cpt import CPT
from mlasengine.objects.borehole import Borehole
from mlasengine.creators.geomodeler import GeoModeler
from mlasengine.algorithms.add_phreatic_line import AlgorithmAddPhreaticLine, AlgorithmSettingsAddPhreaticLine
from mlasengine.algorithms.add_bishop_brute_force import AlgorithmAddBishopBruteForce, AlgorithmSettingsDStabilityBishopBruteForce
from mlasengine.algorithms.add_spencer_genetic import AlgorithmAddSpencerGenetic, AlgorithmSettingsDStabilitySpencerGenetic
from mlasengine.algorithms.add_trafficload import AlgorithmAddTrafficLoad, AlgorithmSettingsTrafficLoad
from mlasengine.helpers import longcode_to_shortcode

from settings import PATH_MLAS, DIR_RASTERDATA, DIR_HEIGHTDATA
from database import Database
from tileset import Tileset, TilesetType, Tile


database = Database()
POINT_GRANULARITY = 0.5
DEFAULT_RDP_EPSILON = 0.1

QCMAX = 30
RFMAX = 10
SCALE_CPT = 20 # every cpt takes up 1/20 of the width of the plot
SCALE_BOREHOLE = 40 # every borehole takes up 1/20 of the width of the plot

# parameters for the normative algorithm
OFFSET_FROM_REFERENCELINE = 5 # where to start weighing the crosssection
LENGTH_FOR_WEIGHTS = 20       # what section length (from offset) to assess for the weight

def generate_geotechnical_profile(
    owner: str, 
    levee_code, 
    cpts_crest: List[CPT] = [],
    cpts_polder: List[CPT] = [],
    boreholes_crest: List[CPT] = [],
    boreholes_polder: List[Borehole] = [],
    output_path: str=""
    ):    
    route = database.get_route(owner, levee_code)
    if route is None:
        raise ValueError(f"Could not find route for levee code'{levee_code}' please check the database.")

    soilcollection = database.get_soils_as_soilcollection(owner)
    
    # get limits
    csmin = route.min_chainage
    csmax = route.max_chainage * 1.1
    zmin = min([cpt.bottom for cpt in cpts_crest + cpts_polder]) 
    zmax = max([cpt.top for cpt in cpts_crest + cpts_polder]) 
    zmin = min(zmin, min([borehole.bottom for borehole in boreholes_crest + boreholes_polder])) - 2.0
    zmax = max(zmax, max([borehole.top for borehole in boreholes_crest + boreholes_polder])) + 2.0
    
    fig, axs = pyplot.subplots(2, figsize=(30, 20))
    
    routepoints = route.regular_spaced_points()
    topline_crest = []
    topline_polder = []

    for cpt in cpts_crest:
        c1, dlmin = 0, 1e9
        for p in routepoints:
            dl = (p[1] - cpt.x)**2 + (p[2] - cpt.y)**2
            if dl < dlmin:
                c1 = p[0]
                dlmin = dl
        
        topline_crest.append((c1, cpt.top))

        c2 = c1 + 1/SCALE_CPT * (csmax - csmin)
        df = cpt.as_dataframe()
        df.loc[df['Rf']>RFMAX,'Rf'] = RFMAX
        df['qcplot'] = c1 + (df['qc'] / QCMAX) * (c2 - c1)
        df['rfplot'] = c2 - (df['Rf'] / RFMAX) * (c2 - c1) * 0.33

        axs[0].plot(df['qcplot'], df['z'], 'k')
        axs[0].plot(df['rfplot'], df['z'], 'k--')      
        axs[0].text(c1, cpt.top + 0.5, cpt.name)  

        soillayers = cpt.to_soillayers()
        for soillayer in soillayers:
            color = soilcollection.get(soillayer.soilcode).color
            p = MPolygon([(c1, soillayer.bottom), (c1, soillayer.top), (c2, soillayer.top), (c2, soillayer.bottom)], facecolor = color, alpha=0.8)
            axs[0].add_patch(p)


    axs[0].set_ylim(zmin, zmax)
    axs[0].set_xlim(csmin, csmax)
    axs[0].grid(b=True)

    for cpt in cpts_polder:
        c1, dlmin = 0, 1e9
        for p in routepoints:
            dl = (p[1] - cpt.x)**2 + (p[2] - cpt.y)**2
            if dl < dlmin:
                c1 = p[0]
                dlmin = dl

        topline_polder.append((c1, cpt.top))
        
        c2 = c1 + 1/SCALE_CPT * (csmax - csmin)
        df = cpt.as_dataframe()
        df.loc[df['Rf']>RFMAX,'Rf'] = RFMAX 
        df['qcplot'] = c1 + df['qc'] / QCMAX * (c2 - c1)
        df['rfplot'] = c2 - df['Rf'] / RFMAX * (c2 - c1) * 0.33

        axs[1].plot(df['qcplot'], df['z'], 'k')
        axs[1].plot(df['rfplot'], df['z'], 'k--')  
        axs[1].text(c1, cpt.top + 0.5, cpt.name)   

        soillayers = cpt.to_soillayers()
        for soillayer in soillayers:
            color = soilcollection.get(soillayer.soilcode).color
            p = MPolygon([(c1, soillayer.bottom), (c1, soillayer.top), (c2, soillayer.top), (c2, soillayer.bottom)], facecolor = color, alpha=0.8)
            axs[1].add_patch(p)   

    for borehole in boreholes_crest:
        c1, dlmin = 0, 1e9
        for p in routepoints:
            dl = (p[1] - borehole.x)**2 + (p[2] - borehole.y)**2
            if dl < dlmin:
                c1 = p[0]
                dlmin = dl

        topline_crest.append((c1, borehole.top))
        
        c2 = c1 + 1/SCALE_BOREHOLE * (csmax - csmin)
        axs[0].text(c1, borehole.top + 0.5, borehole.name)   
        for soillayer in borehole.soillayers:
            shortcode = longcode_to_shortcode(soillayer.soilcode)
            color = soilcollection.get(shortcode).color
            p = MPolygon([(c1, soillayer.bottom), (c1, soillayer.top), (c2, soillayer.top), (c2, soillayer.bottom)], facecolor = color, alpha=0.8)
            axs[0].add_patch(p)   

    for borehole in boreholes_polder:
        c1, dlmin = 0, 1e9
        for p in routepoints:
            dl = (p[1] - borehole.x)**2 + (p[2] - borehole.y)**2
            if dl < dlmin:
                c1 = p[0]
                dlmin = dl

        topline_polder.append((c1, borehole.top))
        
        c2 = c1 + 1/SCALE_BOREHOLE * (csmax - csmin)
        axs[1].text(c1, borehole.top + 0.5, borehole.name)   
        for soillayer in borehole.soillayers:
            shortcode = longcode_to_shortcode(soillayer.soilcode)
            color = soilcollection.get(shortcode).color
            p = MPolygon([(c1, soillayer.bottom), (c1, soillayer.top), (c2, soillayer.top), (c2, soillayer.bottom)], facecolor = color, alpha=0.8)
            axs[1].add_patch(p)      

    axs[1].set_ylim(zmin, zmax)
    axs[1].set_xlim(csmin, csmax)
    axs[1].grid(b=True)
    
    topline_crest = sorted(topline_crest, key = lambda x: x[0])
    axs[0].plot([p[0] for p in topline_crest], [p[1] for p in topline_crest], 'k--')

    topline_polder = sorted(topline_polder, key = lambda x: x[0])
    axs[1].plot([p[0] for p in topline_polder], [p[1] for p in topline_polder], 'k--')
    
    pyplot.tight_layout()
    filename = str(Path(output_path) / f"{levee_code}_geoprofile.png")    
    fig.savefig(filename) 


def generate_crosssections(
        owner: str, 
        levee_code: str, 
        update: bool = False,
        x_water: int = 20,
        x_polder: int = 50,
        apply_rdp: bool = False,  
        rdp_epsilon: float = DEFAULT_RDP_EPSILON,      
        granularity: int = 10
    ):
    """
    Generate crosssections for the given levee and add them to the database

    Args:
        owner (str): owner of the crosssections
        levee_code (str): the levee code 
        update (bool): if true will update existing crosssections in the database
        x_water (int): number of meters towards the water from the referenceline, default to 20
        x_polder (int): number of meters towards the polder from the referenceline, default to 50
        apply_rdp (bool): apply RDP algorithm, defaults to True
        rdp_epsilon (float): apply given epsilon to RDP algorithm, defaults to DEFAULT_RDP_EPSILON in generate.py
        granularity (int): distance to apply between two crosssections

    Returns:
        None
    """    
    route = database.get_route(owner, levee_code)
    if route is None:
        raise ValueError(f"Could not find route for levee code'{levee_code}' please check the database.")

    heightdatafile = Path(PATH_MLAS) / owner / DIR_RASTERDATA / DIR_HEIGHTDATA / f"{levee_code.upper()}.tif"
    if not heightdatafile.is_file():
        raise ValueError(f"Could not find height data file '{heightdatafile}'")

    heighttile = Tile.from_tif(heightdatafile)  
    ditchtiles = Tileset(owner=owner, tileset_type=TilesetType.DITCHES)
    waterbottomtiles = Tileset(owner=owner, tileset_type=TilesetType.WATERBOTTOM)

    chs = [int(ch) for ch in np.arange(route.min_chainage, route.max_chainage, granularity)]
    for ch in tqdm(chs):
        crs_in_database = database.has_crosssection(owner, levee_code, ch)

        if crs_in_database and not update:
            continue

        crosssection = Crosssection(source=DIR_HEIGHTDATA, levee_code=levee_code, levee_chainage=ch)   
        x,y,a = route.xya_at_m(ch)
            
        al = a - math.radians(90)
        ar = a + math.radians(90)
        xl = x + x_water * math.cos(al)
        yl = y + x_water * math.sin(al)
        xr = x + x_polder * math.cos(ar)
        yr = y + x_polder * math.sin(ar)

        dl = x_water + x_polder

        ls = list(np.arange(0, dl, POINT_GRANULARITY)) + [dl]

        pts = []
        for l in ls:
            xp = xl + (l / dl) * (xr - xl)
            yp = yl + (l / dl) * (yr - yl)
            z_ahn = np.round(heighttile.get_z(xp, yp),2)
            z_sloot = np.round(ditchtiles.get_z(xp, yp),2)
            z_wbodem = np.round(waterbottomtiles.get_z(xp, yp),2)
            pts.append((l - x_water,np.round(xp,2),np.round(yp,2),z_ahn,z_sloot,z_wbodem))

        # remove none except for left and right points
        pts = [pts[0]] + [p for p in pts[1:-1] if not np.isnan(p[3]) or not np.isnan(p[4]) or not np.isnan(p[5])] + [pts[-1]]
        
        if len(pts) < 2:
            continue

        # determine the z coord to be used
        cpts = []
        for p in pts: 
            # wbodem first, sloot second, ahn third
            if not np.isnan(p[5]):
                cpts.append([p[0],p[1],p[2],p[5],PointType.WATERBOTTOM])
            elif not np.isnan(p[4]):
                cpts.append([p[0],p[1],p[2],p[4],PointType.DITCH])
            else:
                if l==0:
                    cpts.append([p[0],p[1],p[2],p[3],PointType.REFERENCEPOINT])
                else:
                    cpts.append([p[0],p[1],p[2],p[3],PointType.NONE])
        
        # if the leftmost point has no valid z coordinate then copy the z of the next point
        if np.isnan(cpts[0][3]):
            cpts[0][3] = cpts[1][3]
        if np.isnan(cpts[-1][3]):        
            cpts[-1][3] = cpts[-2][3]

        for p in cpts:
            if not np.isnan(p[3]) or p[4] != PointType.NONE:
                crosssection.points.append(Point3D(
                    l = p[0],
                    x = p[1],
                    y = p[2],
                    z = p[3],
                    point_type = p[4]
                ))     

        if apply_rdp is not None:
            crosssection.rdp(rdp_epsilon=rdp_epsilon)
        crosssection.add_referenceline_at_l(0.0)

        if update:
            database.update_crosssection(owner, crosssection)
        else:
            database.add_crosssection(owner, crosssection)

def normative_crosssections(owner: str, levee_code: str, limits: List[Tuple[int, int]], num_results: int=3, output_path: str="", plot: bool=False) -> List[Crosssection]:
    """
    Get the normative crosssections for the given levee code and limits.

    Args:
        owner (str): owner of the crosssections
        levee_code (str): the code of the levee
        limits (List[Tuple[int, int]]): list with start and end chainage (for example [(0,100), (100, 300)] etc)
        num_results (int): the number of crosssections to return, defaults to 3
        output_path (str): if set this will export the crosssections to the given path, defaults to "" (no output)
        plot_result (bool): plot the result (only works if outputpath is set), defaults to False

    Returns:
        List[Crosssection] ordered by their weight (if output path is set this will also write the result to the given path)
    """
    result = []
    # only plot if there is an output path
    plot = plot and not output_path == ""
    for start, end in limits:
        crosssections = database.get_crosssections(owner, levee_code, start, end)

        # find limits
        zmin, zmax = 1e9, 9999
        zmin = min([min(zmin, crs.bottom) for crs in crosssections]) - 1.0

        selectionweighted = []

        if plot:
            fig = plt.figure.Figure(figsize=(15, 6))
            ax = fig.add_subplot()
        
        for crs in crosssections:
            # determine the weight
            ls = np.linspace(crs.reference_point.l + OFFSET_FROM_REFERENCELINE, crs.reference_point.l + OFFSET_FROM_REFERENCELINE + LENGTH_FOR_WEIGHTS, LENGTH_FOR_WEIGHTS)
            crspoints = [[p.l, p.z] for p in crs.points]
            crspoints += [[crspoints[-1][0], zmin], [crspoints[0][0], zmin]]
            crspolygon = Polygon(crspoints)

            if plot:
                xs = [p.l for p in crs.points]
                ys = [p.z for p in crs.points]
                ax.plot(xs, ys, 'k:')

            zmax = crs.top + 0.1
            weight = 0.0
            for i in range(1, len(ls)):
                lmin = ls[i-1]
                lmax = ls[i]
                lmid = (lmin + lmax) / 2.0
                rect = Polygon([(lmin, zmax), (lmax,zmax), (lmax,zmin), (lmin,zmin)])

                ipolygon = rect.intersection(crspolygon)

                #ax.plot(*ipolygon.exterior.xy, 'C7:')

                weight += pow(ipolygon.area,3) # * (lmid - lmin) / LENGTH_FOR_WEIGHTS

            # add the crosssection and the weight
            selectionweighted.append((weight, crs))

        # sort, lowest weight first
        selectionweighted = sorted(selectionweighted, key=lambda x:x[0])

        if plot:
            for _, crs in selectionweighted[:num_results]:
                xs = [p.l for p in crs.points]
                ys = [p.z for p in crs.points]
                ax.plot(xs, ys, label=f"{crs.levee_chainage:04d}")

        if output_path != "":
            for w, crs in selectionweighted[:num_results]:                
                crs.to_csv_2d(output_path, f"{levee_code}_{crs.levee_chainage:04d}.csv")

        if plot:
            ax.set_title(f"{levee_code} maatgevende profielen tussen {start} en {end}m")
            fig.legend()                  
            filename = str(Path(output_path) / f"{levee_code}_{start}_{end}.png")    
            fig.savefig(filename)                

        # return the normative crosssections
        result += [e[1] for e in selectionweighted[:num_results]]
    return result

def normative_soilinvestigation(owner: str, cpts: List[CPT] = [], boreholes: List[Borehole] = [], minimum_depth: float = 3.0, num_results: int=1):
    """
    Get the normative soil investigation.

    Args:
        owner (str): owner of the crosssections
        cpts (List[CPT]): list of CPTs to look at
        boreholes (List[Borehole]): list of boreholes to look at
        minimum_depth (float): height of the soillayers to check from the top of the soil investigation
        num_results (int): the number of soil investigations to return ordered by their weight (lower = worse), defaults to 1 

    Returns:
        List[Union[Borehole, CPT]] ordered by their weight (lower is worse)
    """
    # TODO > add boreholes in the equation

    db = Database()
    soilcollection = db.get_soils_as_soilcollection(owner)
    

    result = []
    for cpt in cpts:
        soillayers = cpt.to_soillayers()

        top = soillayers[0].top
        bottom = top - minimum_depth

        if soillayers[-1].bottom > bottom:
            raise ValueError(f"Trying to get the normative soilinvestigation where the minimum depth {top - minimum_depth} exceeds the cpt depth {soillayers[-1].bottom}.")

        csum, phisum = 0.0, 0.0
        for soillayer in soillayers:
            if soillayer.top > bottom:
                if not soilcollection.has_soilcode(soillayer.soilcode):
                    raise ValueError(f"Missing soilcode '{soillayer.soilcode}' in soilcollection.")    
                soil = soilcollection.get(soillayer.soilcode)

                if soillayer.bottom > bottom:         
                    zmid = (soillayer.top - soillayer.bottom) / 2.0         
                    zweight = minimum_depth + (top - zmid)
                    csum += zweight * soil.cohesion * (soillayer.top - soillayer.bottom)
                    phisum += zweight * soil.friction_angle / 10.0 * (soillayer.top - soillayer.bottom)
                else:
                    zmid = (soillayer.top - bottom) / 2.0         
                    zweight = minimum_depth + (top - zmid)
                    csum += zweight * soil.cohesion * (soillayer.top - bottom)
                    phisum += zweight * soil.friction_angle / 10.0 * (soillayer.top - bottom)

        result.append((csum + phisum, cpt))

    for borehole in boreholes:
        soillayers = borehole.soillayers        

        top = soillayers[0].top
        bottom = top - minimum_depth

        if soillayers[-1].bottom > bottom:
            raise ValueError(f"Trying to get the normative soilinvestigation where the minimum depth {top - minimum_depth} exceeds the cpt depth {soillayers[-1].bottom}.")

        csum, phisum = 0.0, 0.0
        for soillayer in soillayers:
            soilcode = longcode_to_shortcode(soillayer.soilcode)
            if soillayer.top > bottom:
                if not soilcollection.has_soilcode(soilcode):
                    raise ValueError(f"Missing soilcode '{soilcode}' in soilcollection.")    
                soil = soilcollection.get(soilcode)

                if soillayer.bottom > bottom:         
                    zmid = (soillayer.top - soillayer.bottom) / 2.0         
                    zweight = minimum_depth + (top - zmid)
                    csum += zweight * soil.cohesion * (soillayer.top - soillayer.bottom)
                    phisum += zweight * soil.friction_angle / 10.0 * (soillayer.top - soillayer.bottom)
                else:
                    zmid = (soillayer.top - bottom) / 2.0         
                    zweight = minimum_depth + (top - zmid)
                    csum += zweight * soil.cohesion * (soillayer.top - bottom)
                    phisum += zweight * soil.friction_angle / 10.0 * (soillayer.top - bottom)

        result.append((csum + phisum, borehole))

    result = sorted(result, key=lambda x:x[0])    

    if len(result) < num_results:
        return [si[1] for si in result]
    else:
        return [si[1] for si in result[:num_results]]

def stix_from_soilinvestigation(
        owner: str,
        crosssection: Crosssection, 
        fillmaterial_top: str,        
        object_left: Union[CPT, Borehole],         
        object_right: Union[CPT, Borehole] = None,        
        pl_settings: AlgorithmSettingsAddPhreaticLine = None,
        bbf_settings: AlgorithmSettingsDStabilityBishopBruteForce = None,
        sga_settings: AlgorithmSettingsDStabilitySpencerGenetic = None,
        tl_settings: AlgorithmSettingsTrafficLoad = None,
        output_path: str=""
    ) -> io.BytesIO:
    """
    Create a stix file from the given information

    Args:
        crosssection (Crosssection): the crosssection to use
        fillmaterial_top (str): soilcode of the soil to be used as a top material if the top of the CPT / borehole is below the highest point of the crosssection
        object_left (Union[CPT, Borehole]): the CPT or borehole to use for the left side (water) of the levee
        object_right (Union[CPT, Borehole]): the CPT or borehole to use for the right side (polder) of the levee, if not set then the left will be used
        pl_settings
        bbf_settings
        tl_settings
        output_path (str): filepath to save the stix file to, defaults to "" (no output)

    Returns:
        io.Bytes: stream with the stix file
    """
    soilcollection = database.get_soils_as_soilcollection(owner)
    if not soilcollection.has_soilcode(fillmaterial_top):
        raise ValueError(f"Got unknown soilcode '{fillmaterial_top}' for fillmaterial_top check the database for valid names.")

    xl = crosssection.startpoint.l
    xr = crosssection.endpoint.l
    xt = crosssection.l_transition

    if xt == -9999:
        try:
            crosssection.add_transition()
            xt = crosssection.l_transition
        except:
            pass

    if object_right is None:
        xt = xr
    
    geom = GeoModeler(
        soilcollection = soilcollection,        
        crosssection = crosssection,
        debug = False
    )

    if isinstance(object_left, CPT):
        geom.add_from_cpt(left=xl, right=xt, cpt=object_left, fill_soillayer_code='clay')
    elif isinstance(object_left, Borehole):
        geom.add_from_borehole(left=xl, right=xt, borehole=object_left, fill_soillayer_code='clay')
    
    if object_right:
        if isinstance(object_right, CPT):
            geom.add_from_cpt(left=xt, right=xr, cpt=object_right, fill_soillayer_code='clay')
        elif isinstance(object_right, Borehole):
            geom.add_from_borehole(left=xt, right=xr, borehole=object_right, fill_soillayer_code='clay')

    

    if crosssection.top > object_left.z_top and fillmaterial_top is None:
        raise ValueError(f"Warning, the crosssection is higher than the highest point on the left soil investigation and no filllayertop has been set")        
    if object_right is not None and crosssection.top > object_right.z_top and fillmaterial_top is None:    
        raise ValueError(f"Warning, the crosssection is higher than the highest point on the right soil investigation and no filllayertop has been set")        

    geom.cut_from_crosssection()
    geom.solve()
    cm = geom.to_calculationmodel()

    prefix = ""

    if pl_settings:
        algo_pl = AlgorithmAddPhreaticLine(
            calculation_model = cm,
            settings = pl_settings
        )
        cm = algo_pl.execute()

    if tl_settings:
        algo_tl = AlgorithmAddTrafficLoad(
            calculation_model = cm,
            settings = tl_settings
        )
        cm = algo_tl.execute()

    if bbf_settings:
        prefix = "bbf"
        algo_bbf = AlgorithmAddBishopBruteForce(
            calculation_model = cm, 
            settings=bbf_settings
        )
        cm = algo_bbf.execute()

    if sga_settings:
        prefix = "sga"
        algo_sa = AlgorithmAddSpencerGenetic(
            calculation_model = cm, 
            settings=sga_settings
        )
        cm = algo_sa.execute() 
    
    
    dm = cm.to_dstability_model()

    if output_path != "":
        filename = Path(output_path) / f"{prefix}_{crosssection.levee_code}_{crosssection.levee_chainage:04d}.stix"
        dm.serialize(filename)
    else:
        return dm.serialize_buffer()

if __name__ == "__main__":
    database = Database()
    from mlasengine.helpers import case_insensitive_glob

    cpts_crest, cpts_polder = [], []
    boreholes_crest, boreholes_polder = [], []

    for cptfile in case_insensitive_glob("D:/_TEMP/P1011/kruin/cpts", '.gef'):
        cpts_crest.append(CPT.from_file(cptfile))
    for cptfile in case_insensitive_glob("D:/_TEMP/P1011/polder/cpts", '.gef'):
        cpts_polder.append(CPT.from_file(cptfile))
    for boreholefile in case_insensitive_glob("D:/_TEMP/P1011/kruin/boreholes", '.gef'):
        boreholes_crest.append(Borehole.from_file(boreholefile))
    for boreholefile in case_insensitive_glob("D:/_TEMP/P1011/polder/boreholes", '.gef'):
        boreholes_polder.append(Borehole.from_file(boreholefile))
    
    generate_geotechnical_profile(
        owner = "waternet", 
        levee_code = "P1011",
        cpts_crest = cpts_crest,
        cpts_polder = cpts_polder,
        boreholes_crest = boreholes_crest,
        boreholes_polder = boreholes_polder,
        output_path = "D:/_TEMP/P1011"
    )


    # db = Database()
    # cpts, boreholes = [], [] 

    # cpts.append(db.get_cpt_by_id(15479))
    # cpts.append(db.get_cpt_by_id(15481))
    # cpts.append(db.get_cpt_by_id(15482))
    # boreholes.append(db.get_borehole_by_id(228))
    # boreholes.append(db.get_borehole_by_id(229))

    # norm_si = normative_soilinvestigation(owner='waternet', cpts=cpts, boreholes=boreholes)
    # print(norm_si)


    # generate_crosssections('waternet', 'A123')
    # normative_crosssections(
    #     owner='waternet',
    #     levee_code='A117',
    #     limits = [(0,1160),(1160,9999)],
    #     output_path="D:/_TEMP",
    #     plot=True
    # )
    
    
    
    
    # # load crosssection
    # crs = db.get_crosssection('waternet', 'A123', 1290)
    
    # # load soil investigations
    # cpt_left = CPT.from_file("D:/Data/Documents/mlas/waternet/grondonderzoek/sonderingen/VAK_B02/B02-631.gef")
    # cpt_right = CPT.from_file("D:/Data/Documents/mlas/waternet/grondonderzoek/sonderingen/VAK_B02/B02-635.gef")

    # # define some parameters for the phreatic line
    # pl_settings = AlgorithmSettingsAddPhreaticLine(
    #     waterlevel = 0.0,
    #     waterlevel_polder = -2.5,
    #     offset_points = [Point2D(x=3, z=-1.0)]
    # )

    # bbf_settings = AlgorithmSettingsDStabilityBishopBruteForce(
    #     searcharea_min_width = 10, 
    #     searcharea_min_height = 10,
    #     searcharea_resolution = 1,
    #     tangentarea_zmin = -8,
    #     tangentarea_min_height = 5,
    #     tangentarea_resolution = 1
    # )

    # tl_settings = AlgorithmSettingsTrafficLoad(
    #     name = "verkeer",
    #     offset = -3.0,
    #     magnitude = 13.0,
    #     width = 2.5,
    #     distribution_angle = 45.0        
    # )
    
    # stix_from_soilinvestigation(
    #     owner='waternet',
    #     crosssection=crs, 
    #     object_left = cpt_left, 
    #     object_right = cpt_right,
    #     fillmaterial_top='clay',
    #     pl_settings=pl_settings,
    #     bbf_settings=bbf_settings,
    #     tl_settings=tl_settings,
    #     output_path="D:\_TEMP"        
    # )


    



