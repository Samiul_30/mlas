from typing import List, Dict
from numpy import short
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, UnicodeText, Float, create_engine, func
from sqlalchemy.sql.expression import null
from sqlalchemy.orm import sessionmaker
from geoalchemy2 import Geometry
from geoalchemy2.shape import to_shape
from pyproj import Transformer
import logging
from pathlib import Path
from mlasengine.helpers import longcode_to_shortcode

from mlasengine.objects.cpt import CPT
from mlasengine.objects.borehole import Borehole
from mlasengine.objects.crosssection import Crosssection
from mlasengine.objects.soil import Soil
from mlasengine.objects.soilcollection import SoilCollection

from secrets import DB_HOST, DB_USER, DB_NAME, DB_PASSWORD
from settings import PATH_MLAS
from route import Route

LOG_DATABASE = Path(PATH_MLAS) / "logs/database.log"
transformer = Transformer.from_crs(28992, 4326, always_xy=True)
Base = declarative_base()

class DB_CPT(Base):
    __tablename__ = "cpts"
    id = Column(Integer, primary_key=True)
    geom = Column(Geometry(geometry_type='POINT', srid=28992), nullable=False)
    owner = Column(String, nullable=False)
    name = Column(String, nullable=False)
    top = Column(Float(precision=2), nullable=False)
    date = Column(String, nullable=False)
    raw = Column(UnicodeText, nullable=False)

class DB_Borehole(Base):
    __tablename__ = "boreholes"
    id = Column(Integer, primary_key=True)
    geom = Column(Geometry(geometry_type='POINT', srid=28992), nullable=False)
    owner = Column(String, nullable=False)
    name = Column(String, nullable=False)
    top = Column(Float(precision=2), nullable=False)
    date = Column(String, nullable=False)
    raw = Column(UnicodeText, nullable=False)

class DB_Crosssection(Base):
    __tablename__ = "crosssections"
    id = Column(Integer, primary_key=True)
    geom = Column(Geometry(geometry_type='LINESTRING', srid=28992), nullable=False)
    owner = Column(String, nullable=False)
    levee_code = Column(String, nullable=False)
    levee_chainage = Column(Integer, nullable=False)
    source = Column(String, nullable=False)
    json = Column(UnicodeText, nullable=False)

# class DB_SoilCodeConversion(Base):
#     __tablename__ = "soilcodeconversions"
#     id = Column(Integer, primary_key=True)
#     owner = Column(String, nullable=False)
#     shortcode = Column(String, nullable=False)
#     longcode = Column(String, nullable=False)

class DB_Soil(Base):
    __tablename__ = "soils"
    id = Column(Integer, primary_key=True)
    owner = Column(String, nullable=False)
    code = Column(String, nullable=False)
    color = Column(String, nullable=False)
    y_dry = Column(Float(precision=2), nullable=False)
    y_sat = Column(Float(precision=2), nullable=False)
    cohesion = Column(Float(precision=2), nullable=False)
    friction_angle = Column(Float(precision=2), nullable=False)
    dilatancy_angle = Column(Float(precision=2), nullable=False)

class DB_Route(Base):
    __tablename__ = "routes"
    id = Column(Integer, primary_key=True)
    geom = Column(Geometry(geometry_type='LINESTRING', srid=28992), nullable=False)    
    owner = Column(String, nullable=False)
    code = Column(String, nullable=False)
    name = Column(String, nullable=False)

class Database:
    def __init__(self):
        logging.basicConfig(filename=LOG_DATABASE, encoding='utf-8', level=logging.DEBUG)
        self._engine = create_engine(f'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:5432/{DB_NAME}', echo=False)
        Session = sessionmaker(bind=self._engine)
        self._session = Session()


    def create(self):        
        DB_CPT.__table__.create(self._engine)
        DB_Borehole.__table__.create(self._engine)
        DB_Crosssection.__table__.create(self._engine)
        DB_Soil.__table__.create(self._engine)
        DB_Route.__table__.create(self._engine)


    #####################################
    # ADD                               #
    # no checks for existing entries!   #
    #####################################
    def add_cpt(self, owner: str, cptfile: str):        
        try:
            cpt = CPT.from_file(cptfile)
            cptdata = open(cptfile, 'r').read()            
            newcpt = DB_CPT(
                geom = f'SRID=28992;POINT({cpt.x} {cpt.y})',
                owner = owner,
                name = cpt.name,
                top = cpt.top,
                date = cpt.date,
                raw = cptdata
            )            
            self._session.add(newcpt)  
            self._session.commit()      
        except Exception as e:
            logging.error(f"Error adding cpt from file '{cptfile}', got error '{e}'")
            self._session.rollback()

    def add_borehole(self, owner: str, boreholefile: str): 
        try:
            borehole = Borehole.from_file(boreholefile)

            # TODO add soil with shortcode
            for soillayer in borehole.soillayers:
                shortcode = longcode_to_shortcode(soillayer.soilcode)
                if not self.has_soil(owner, shortcode):
                    soil = Soil()
                    soil.code = shortcode
                    soil.ydry, soil.ysat = bhscc._soilcode_to_weight(shortcode)
                    soil.color = bhscc._soilcode_to_color(shortcode)
                    soil.cohesion = 0.0
                    soil.dilatancy_angle = 0.0
                    soil.friction_angle = 0.0
                    self.add_soil(owner, soil)
            
            
            boreholedata = open(boreholefile, 'r').read()            
            newborehole = DB_Borehole(
                geom = f'SRID=28992;POINT({borehole.x} {borehole.y})',
                owner = owner,
                name = borehole.name,
                top = borehole.top,
                date = borehole.date,
                raw = boreholedata
            )            
            self._session.add(newborehole)  
            self._session.commit()      
        except Exception as e:
            logging.error(f"Error adding borehole from file '{boreholefile}', got error '{e}'")
            self._session.rollback()

    def add_crosssection(self, owner: str, crosssection: Crosssection):
        try:
            linestring = crosssection.to_linestring()
            newcrosssection = DB_Crosssection(
                geom = f'SRID=28992;{linestring}',
                owner = owner,
                levee_code = crosssection.levee_code,
                levee_chainage = crosssection.levee_chainage,
                source = crosssection.source,
                json = crosssection.to_json(indent=0)
            )
            self._session.add(newcrosssection)  
            self._session.commit() 
        except Exception as e:
            logging.error(f"Error adding crosssection with leveecode '{crosssection.levee_code}' and chainage {crosssection.levee_chainage}, got error '{e}'")
            self._session.rollback()

    def add_route(self, owner: str, route: Route):
        # convert points to shape linestring
        try:
            linestring = route.to_linestring()
            newroute = DB_Route(
                geom = f'SRID=28992;{linestring}',
                owner = owner,            
                code = route.code,
                name = route.name            
            )
            self._session.add(newroute)  
            self._session.commit()      
        except Exception as e:
            logging.error(f"Error adding route with code '{route.code}', got error '{e}'")
            self._session.rollback()


    # def add_soilcodeconversion(self, owner: str, soilcode: str, shortcode: str):
    #     currentdict = self.get_soilcodeconversions(owner=owner)
    #     if not soilcode in currentdict.keys():
    #         newscc = DB_SoilCodeConversion(
    #             owner = owner,
    #             shortcode = shortcode,
    #             longcode = soilcode                
    #         )
    #         self._session.add(newscc)
    #         self._session.commit()

    
    # def add_soilcodeconversions(self, owner: str, soilcodes: Dict):
    #     """
    #     Add soilcode conversion dict to the database. The shortcode can be created using
    #     the mlasengine BoreholeSoilCodeConvertor class. The longcode is the name as found 
    #     in the borehole file (with rules applied in the borehole reader to convert spaces
    #     and comma's as defined in the borehole read function)

    #     Args:
    #         owner (str): owner of the soilcodes
    #         soilcodes (Dict): dictionary with key = longcode, value = shortcode

    #     Returns:
    #         None
    #     """
    #     currentdict = self.get_soilcodeconversions(owner=owner)
    #     added = False
    #     for k, v in soilcodes.items():
    #         if not k in currentdict.keys():
    #             newscc = DB_SoilCodeConversion(
    #                 owner = owner,
    #                 shortcode = v,
    #                 longcode = k                
    #             )
    #             self._session.add(newscc)  
    #             added = True

    #     if added:
    #         self._session.commit()  

    def add_soil(self, owner: str, soil: Soil) -> None:
        newsoil = DB_Soil(
            owner = owner,            
            code = soil.code,
            color = soil.color,
            y_dry = soil.y_dry,
            y_sat = soil.y_sat,
            cohesion = soil.cohesion,
            friction_angle = soil.friction_angle,
            dilatancy_angle = soil.dilatancy_angle,
        )            
        self._session.add(newsoil)  
        self._session.commit() 

    

    #########################
    # GET                   #
    #########################
    def get_soils_as_soilcollection(self, owner: str) -> SoilCollection:
        # default soils
        result = SoilCollection()
        
        # add those from database
        soils = self.get_soils(owner)
        return SoilCollection(soils=soils)
    
    def get_soils(self, owner: str) -> List[Soil]:
        query = self._session.query(DB_Soil).\
            filter(DB_Soil.owner == owner)
        
        result = []
        for instance in query:
            result.append(Soil(
                code = instance.code,
                color = instance.color,
                y_dry = instance.y_dry,
                y_sat = instance.y_sat,
                cohesion = instance.cohesion,
                friction_angle = instance.friction_angle,
                dilatancy_angle = instance.dilatancy_angle,
            ))
        return result
    
    # def get_soilcodeconversions(self, owner: str) -> Dict:        
    #     query = self._session.query(DB_SoilCodeConversion).\
    #         filter(DB_SoilCodeConversion.owner == owner)    

    #     return {instance.longcode:instance.shortcode for instance in query}

    
    def get_cpt_by_id(self, id) -> CPT:
        """Get CPT with the given id

        Args:
            id (int): id of the CPT in the database

        Returns:
            CPT or None
        """
        query = self._session.query(DB_CPT).\
            filter(DB_CPT.id == id)

        result = None
        if query.count() > 0:
            instance = query[0]
            try:
                cpt = CPT.from_raw(instance.raw)
                return cpt
            except Exception as e:
                logging.warning(f"Error convert raw cpt data to CPT from '{instance.id}', got error '{e}'")

        return result

    def get_borehole_by_id(self, id) -> Borehole:
        """Get borehole with the given id

        Args:
            id (int): id of the borehole in the database

        Returns:
            borehole or None
        """
        query = self._session.query(DB_Borehole).\
            filter(DB_Borehole.id == id)

        result = None
        if query.count() > 0:
            instance = query[0]
            try:
                borehole = Borehole.from_raw(instance.raw)
                return borehole
            except Exception as e:
                logging.warning(f"Error convert raw borehole data to borehole from '{instance.id}', got error '{e}'")

        return result
    
    def get_cpts(self, owner: str, x: float, y: float, distance: float) -> List[CPT]:
        """Get CPTs within a distance of the given x, y coordinates
        
        Args:
            owner (str): name of the owner
            x (float): x RD coordinate (srid=28992) in m
            y (float): y RD coordinate (srid=28992) in m
            distance (float): maximum distance from the given point

        Returns:
            List[CPT]: list with the cpt's that match the given parameters
        """
        buffer = func.ST_Buffer(func.ST_GeomFromText(f'SRID=28992;POINT({x} {y})'), distance)
        query = self._session.query(DB_CPT).\
            filter(func.ST_Contains(buffer, DB_CPT.geom)).\
            filter(DB_CPT.owner == owner)
        
        result = []
        for instance in query:
            try:
                cpt = CPT.from_raw(instance.raw)
                result.append(cpt)
            except Exception as e:
                logging.warning(f"Error convert raw cpt data to CPT from '{instance.id}', got error '{e}'")

        return result

    def get_routes(self, owner: str) -> List[Route]:
        """
        Get all routes of the given owner

        Args:
            owner (str): name of the owner

        Return:
            List[Route]
        """
        query = self._session.query(DB_Route).\
            filter(DB_Route.owner == owner)
        
        result = []
        for instance in query:
            shape = to_shape(instance.geom)
            x, y = shape.coords.xy
            points = [(p[0], p[1]) for p in zip(x, y)]
            result.append(Route.from_points(instance.code, instance.name, points))

        return result

    def get_route(self, owner: str, levee_code: str) -> Route:
        """
        Get route by owner and dtcode

        Args:  
            owner (str): name of the owner
            levee_code (str): code of the levee

        Returns:
            Route or None
        """
        query = self._session.query(DB_Route).\
            filter(DB_Route.owner == owner).\
            filter(DB_Route.code == levee_code)

        if query.count() > 0:
            instance = query[0]
            shape = to_shape(instance.geom)
            x, y = shape.coords.xy
            points = [(p[0], p[1]) for p in zip(x, y)]
            return Route.from_points(instance.code, instance.name, points)
        else:
            return None

    def get_all_boreholes(self, owner: str) -> List[Borehole]:
        """
        Get all boreholes from the database
        """
        result = []
        query = self._session.query(DB_Borehole).\
            filter(DB_Borehole.owner == owner)
        
        result = []
        for instance in query:
            try:
                borehole = Borehole.from_raw(instance.raw)
                result.append(borehole)
            except Exception as e:
                logging.warning(f"Error convert raw borehole data to Borehole from '{instance.id}', got error '{e}'")

        return result
    
    def get_boreholes(self, owner: str, x: float, y: float, distance: float) -> List[Borehole]:
        """Get boreholes within a distance of the given x, y coordinates
        
        Args:
            owner (str): name of the owner
            x (float): x RD coordinate (srid=28992) in m
            y (float): y RD coordinate (srid=28992) in m
            distance (float): maximum distance from the given point

        Returns:
            List[Borehole]: list with the boreholes that match the given parameters
        """
        buffer = func.ST_Buffer(func.ST_GeomFromText(f'SRID=28992;POINT({x} {y})'), distance)
        query = self._session.query(DB_Borehole).\
            filter(func.ST_Contains(buffer, DB_Borehole.geom)).\
            filter(DB_Borehole.owner == owner)
        
        result = []
        for instance in query:
            try:
                borehole = Borehole.from_raw(instance.raw)
                result.append(borehole)
            except Exception as e:
                logging.warning(f"Error convert raw borehole data to Borehole from '{instance.id}', got error '{e}'")

        return result

    def get_crosssections(self, owner: str, levee_code: str, start: int=0, end: int=1e9) -> List[Crosssection]:
        """
        Get the crosssections in the database

        Args:
            owner (str): name of the owner
            levee_code (str): code of the levee
            start (int): from chainage, defaults to 0
            end (int): to chainage, defaults to 1e9

        Returns:
            List[Crosssection]: list with the found crosssections
        """
        query = self._session.query(DB_Crosssection).\
            filter(DB_Crosssection.owner == owner).\
            filter(DB_Crosssection.levee_code == levee_code).\
            filter(DB_Crosssection.levee_chainage >= start).\
            filter(DB_Crosssection.levee_chainage <= end)

        result = []
        for instance in query:
            result.append(Crosssection.from_json(instance.json))

        return result

    def get_crosssections_from_chainages(self, owner: str, levee_code: str, chainages: List[int]) -> List[Crosssection]:
        """Get the crosssection from the given code and chainages

        Args:
            owner (str): name of the owner
            levee_code (str): code of the levee
            chainages (List[int]): chainages to find

        Returns:
            List[Crosssection]: the crosssections or empty list if not found
        """
        result = []
        for ch in chainages:
            crs = self.get_crosssection(owner, levee_code, ch) 
            if crs is not None:
                result.append(crs)
        return result

    def get_crosssection(self, owner: str, levee_code: str, chainage: int) -> Crosssection:
        """Get the crosssection from the given code and chainage

        Args:
            owner (str): name of the owner
            levee_code (str): code of the levee
            chainage (int): chainge to find

        Returns:
            Crosssection: the crosssection or None if not found
        """
        query = self._session.query(DB_Crosssection).\
            filter(DB_Crosssection.owner == owner).\
            filter(DB_Crosssection.levee_code == levee_code).\
            filter(DB_Crosssection.levee_chainage == chainage)

        if query.count() > 0:
            instance = query[0]
            return Crosssection.from_json(instance.json)

        return None
        
    #########################
    # HAS                   #
    #########################
    def has_cpt(self, owner: str, cpt: CPT):
        """Check if the given cpt is in the database

        Args:
            owner (str): owner name
            cpt (CPT): the cpt to look for

        Returns:
            bool: true if the CPT is in the database
        """        
        query = self._session.query(DB_CPT).\
            filter(func.ST_Contains(DB_CPT.geom, f'SRID=28992;POINT({cpt.x} {cpt.y})')).\
            filter(DB_CPT.owner == owner)        
        return query.count() > 0

    def has_borehole(self, owner: str, borehole: Borehole):
        """Check if the given borehole is in the database

        Args:
            owner (str): owner name
            borehole (Borehole): the borehole to look for

        Returns:
            bool: true if the borehole is in the database
        """        
        query = self._session.query(DB_Borehole).\
            filter(func.ST_Contains(DB_Borehole.geom, f'SRID=28992;POINT({borehole.x} {borehole.y})')).\
            filter(DB_Borehole.owner == owner)        
        return query.count() > 0

    def has_crosssection(self, owner: str, levee_code: str, levee_chainage: int) -> bool:
        """Check if the given crosssection is in the database

        Args:
            owner (str): owner name
            levee_code (str): code of the levee
            levee_chainage (int): chainage of the levee

        Returns:
            bool: true if the crosssection is in the database
        """        
        query = self._session.query(DB_Crosssection).\
            filter(DB_Crosssection.owner == owner).\
            filter(DB_Crosssection.levee_code == levee_code).\
            filter(DB_Crosssection.levee_chainage == levee_chainage)        
        return query.count() > 0
    
    def has_soil(self, owner: str, soilcode: str) -> bool:
        """Check if the given soil in already in the database

        Args:   
            owner (str): owner name
            soil (str): the soilcode to look for

        Returns:
            bool: true if the soil is in the database
        """
        query = self._session.query(DB_Borehole).\
            filter(DB_Soil.code == soilcode).\
            filter(DB_Soil.owner == owner)        
        return query.count() > 0

    def has_route(self, owner:str, code: str) -> bool:
        """Check if the given route in already in the database

        Args:   
            owner (str): owner name
            code (str): the route code to look for

        Returns:
            bool: true if the route is in the database
        """
        query = self._session.query(DB_Route).\
            filter(DB_Route.code == code).\
            filter(DB_Route.owner == owner)        
        return query.count() > 0

    #########################
    # UPDATE                #
    #########################
    def update_soil(self, owner: str, soil: Soil):
        if self.has_soil(owner, soil.code):
            query = self._session.query(DB_Soil).\
            filter(DB_Soil.code == soil.code).\
            filter(DB_Soil.owner == owner).\
            update(
                {
                    'color':soil.color,
                    'y_dry':soil.y_sat,
                    'y_sat':soil.y_sat,
                    'cohesion':soil.cohesion,
                    'friction_angle':soil.friction_angle,
                    'dilatancy_angle':soil.dilatancy_angle                
                }
            )  

    def update_crosssection(self, owner: str, crosssection: Crosssection):
        if self.has_crosssection(owner, crosssection.levee_code, crosssection.levee_chainage):
            query = self._session.query(DB_Crosssection).\
            filter(DB_Crosssection.owner == owner).\
            filter(DB_Crosssection.levee_code == crosssection.levee_code).\
            filter(DB_Crosssection.levee_chainage == crosssection.levee_chainage).\
            update(
                {
                    'source':crosssection.source,
                    'json':crosssection.to_json()
                }
            )    


if __name__=="__main__":
    db = Database()
    #db.get_routes(owner='waternet')
    #db.create()
    # cpts = db.get_cpts(owner='waternet', x=112084, y=492892, distance=200)
    #i = 1
    #crss = db.get_crosssections('waternet', 'A117')
    #crs = db.get_crosssection('waternet', 'A117', 0)
    soilcollection = db.get_soils_as_soilcollection(owner='waternet')
    i = 1


