from datetime import datetime
from pathlib import Path
from typing import Tuple
from sqlalchemy.sql.expression import update
from tqdm import tqdm
import numpy as np
import rasterio
from rasterio.transform import from_origin
from rasterio.crs import CRS
import glob
import shapefile

from mlasengine.helpers import case_insensitive_glob, soilcode_to_parameters
from mlasengine.objects.borehole import Borehole
from mlasengine.helpers import longcode_to_shortcode
from mlasengine.objects.cpt import CPT
from mlasengine.objects.soil import Soil


from database import Database
from mlasengine.objects.soilcollection import DEFAULT_CPT_SOILS
from settings import DIR_BOREHOLES, OWNERS, PATH_MLAS, DIR_SOILINVESTIGATION, DIR_CPTS
from tileset import Tileset, TilesetType
from route import Route

database = Database()

def update_cpts():
    """
    Update the cpts in the database. 

    Checks all files in the given DIR_SOILINVESTIGATION for every owner in OWNERS and
    uploads new files to the database. Note that the update does not check if CPT
    files are removed. It only adds new CPTs

    Args:
        None
    Returns:
        None
    """
    for owner in OWNERS:
        logfile = open(Path(PATH_MLAS) / owner / "cptimport.log", 'a+')
        path_cpts = Path(PATH_MLAS) / owner / DIR_SOILINVESTIGATION / DIR_CPTS
        cptfiles = case_insensitive_glob(path_cpts, '.gef')

        for fcpt in tqdm(cptfiles):
            try:
                cpt = CPT.from_file(fcpt)

                # skip all invalid coordinates (also the really old Amersfoort based ones)
                if cpt.x < -7000 or cpt.x > 300000 or cpt.y < 289000 or cpt.y > 629000:
                    logfile.write(f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: Skipping CPT file {fcpt}, invalid coordinates\n")

                if not database.has_cpt(owner, cpt):
                    database.add_cpt(owner, fcpt)
            except Exception as e:
                logfile.write(f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: Error importing cptfile {fcpt}, got error {e}\n")
        logfile.close()

def update_boreholes():
    """
    Update the boreholes in the database. 

    Checks all files in the given DIR_SOILINVESTIGATION for every owner in OWNERS and
    uploads new files to the database. Note that the update does not check if borehole
    files are removed. It only adds new boreholes

    Args:
        None
    Returns:
        None
    """
    for owner in OWNERS:
        logfile = open(Path(PATH_MLAS) / owner / "boreholeimport.log", 'a+')
        path_boreholes = Path(PATH_MLAS) / owner / DIR_SOILINVESTIGATION / DIR_BOREHOLES
        boreholefiles = case_insensitive_glob(path_boreholes, '.gef')

        for fborehole in tqdm(boreholefiles):
            try:
                borehole = Borehole.from_file(fborehole)

                # skip all invalid coordinates (also the really old Amersfoort based ones)
                if borehole.x < -7000 or borehole.x > 300000 or borehole.y < 289000 or borehole.y > 629000:
                    logfile.write(f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: Skipping boreholefile {fborehole}, invalid coordinates\n")

                if not database.has_borehole(owner, borehole):
                    database.add_borehole(owner, fborehole)                    
            except Exception as e:
                logfile.write(f"{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: Error importing boreholefile {fborehole}, got error {e}\n")
        logfile.close()

def update_soils(update=False):
    """Update the soils in the database. 

    Checks all soils that are available in the DEFAULT_SOILS from mlasengine as well
    as all possible shortcodes in the soilcodeconversion table using the shortcode 

    The default soils will use the parameters given in the DEFAULT_SOILS dictionary.
    For the shortcode the automated method using the mlasengine helpers are used.

    Args:
        update (bool): if True will overwrite already defined parameters in the database, defaults to False

    Returns:
        None"""    
    # start with the default soils
    for owner in OWNERS:
        currentsoilcodes = [soil.code for soil in database.get_soils(owner)]    
        for soilcode, soil in tqdm(DEFAULT_CPT_SOILS.items()):
            if not soilcode in currentsoilcodes:
                soil = Soil(
                    code=soilcode,
                    color=soil.color,
                    y_dry=soil.y_dry,
                    y_sat=soil.y_sat,
                    cohesion=soil.cohesion,
                    friction_angle=soil.friction_angle,
                    dilatancy_angle=soil.dilatancy_angle
                )
                database.add_soil(owner, soil)
            elif update:
                soil = Soil(
                    code=soilcode,
                    color=soil.color,
                    y_dry=soil.y_dry,
                    y_sat=soil.y_sat,
                    cohesion=soil.cohesion,
                    friction_angle=soil.friction_angle,
                    dilatancy_angle=soil.dilatancy_angle
                )
                database.update_soil(owner, soil)

        # now for the borehole soilcodes
        currentshortcodes = list(set(database.get_soilcodeconversions(owner=owner).values()))
        for soilcode in tqdm(currentshortcodes):
            if not soilcode in currentsoilcodes:
                parameters = soilcode_to_parameters(soilcode)
                soil = Soil(
                    code=soilcode,
                    color=parameters['color'],
                    y_dry=parameters['y_dry'],
                    y_sat=parameters['y_sat'],
                    cohesion=parameters['cohesion'],
                    friction_angle=parameters['friction_angle'],
                    dilatancy_angle=parameters['dilatancy_angle']
                )
                database.add_soil(owner, soil)

            if soilcode in currentsoilcodes and update:
                parameters = soilcode_to_parameters(soilcode)
                soil = Soil(
                    code=soilcode,
                    color=parameters['color'],
                    y_dry=parameters['y_dry'],
                    y_sat=parameters['y_sat'],
                    cohesion=parameters['cohesion'],
                    friction_angle=parameters['friction_angle'],
                    dilatancy_angle=parameters['dilatancy_angle']
                )
                database.update_soil(owner, soil)

def upload_routes_from_shapefile(filename, owner):
    """
    Convenience function to convert a shapefile with linestrings and fields [code, name, owner] to the new routes database table

    THIS FUNCTION WILL ONLY WORK WITH THE FOLLOWING FIELD ORDER
    index, code, name, owner
    """    
    shape = shapefile.Reader(filename)
    for feature in tqdm(shape.shapeRecords()):
        coords = [(p[0], p[1]) for p in feature.shape.__geo_interface__['coordinates']]
        _, code, name, owner = feature.record

        try:
            rt = Route.from_points(code, name, coords)            
        except Exception as e:
            print(f"Error trying to upload code '{code}', error = '{e}'")
            continue

        if not database.has_route(owner, rt.code):            
            database.add_route(owner=owner, route=rt)           


def fix_boreholecodes():
    """Looks for all borehole soillayers names and makes sure that the shortcode 
    is available in the soils collection
    """
    for owner in OWNERS:
        soilcollection = database.get_soils_as_soilcollection(owner)
        currentsoils = soilcollection.all_soilcodes()
        for borehole in tqdm(database.get_all_boreholes(owner)):
            for soillayer in borehole.soillayers:
                shortcode = longcode_to_shortcode(soillayer.soilcode)
                if not shortcode in currentsoils:
                    soil = Soil()
                    parameters = soilcode_to_parameters(shortcode)
                    soil.code = shortcode
                    soil.y_dry = parameters['y_dry']
                    soil.y_sat = parameters['y_sat']
                    soil.color = parameters['color']
                    soil.cohesion = parameters['cohesion']
                    soil.friction_angle = parameters['friction_angle']
                    soil.dilatancy_angle = parameters['dilatancy_angle']                
                    database.add_soil(owner, soil)
                    currentsoils.append(soil.code)

def extract_heightdata(owner, filepath: str):
    """This function will read the given raw height data source as found 
    in the path PATH_MLAS/owner/DIR_RASTERDATA/DIR_RAW_HEIGHTDATA and
    tries to cut out new tif files that are limited to the boundaries
    of the availabe route geometries. These files will be saved to the
    given filepath 

    Note that the route should not extend beyond a maximum of 2x2
    raster files (see manual)

    Args:
        owner (str): owner name
        filepath (str): Path to save the new rasterfiles to

    Returns:
        None
    """
    for owner in OWNERS:
        ahn_data: Tileset = Tileset(owner=owner, tileset_type=TilesetType.RAWHEIGHTDATA)
        routes = database.get_routes(owner = owner)
        
    
        # get all already generated tif files in this directory so we can skip if they exist
        #dtcodes = [s.stem.strip() for s in ] case_insensitive_glob(filepath, '.tif')]
        #i = 0

        # do the magic
        for route in tqdm(routes):
            # if dtcode in dtcodes:
            #     print(f"Already handled {dtcode} so skipping..")
            #     continue    

            # some feedback since reading the tif files can take while
            # print(f"Dealing with levee code; {route.code}")

            # get boundary of the referenceline
            xmin, ymin, xmax, ymax = route.get_bounding_box()
            # and add an offset
            xmin -= 100
            ymin -= 100
            xmax += 100
            ymax += 100

            # this will only work if a levee is max 2x2 tiles which is a valid assumption (I think... ;-)    
            tile_tl = ahn_data.get_tile_by_xy(xmin, ymax)
            tile_tr = ahn_data.get_tile_by_xy(xmax, ymax)
            tile_bl = ahn_data.get_tile_by_xy(xmin, ymin)
            tile_br = ahn_data.get_tile_by_xy(xmax, ymin)

            # start with the data of the topleft tile
            if tile_tl is None:
                print(f"Could not find tiles for {route.code}.")
                continue

            tile_tl._read() # force to read the data and store the data   
            data = tile_tl.data  #np.array

            # if there is another tile on the right and it is not the
            # same as tile_tl add it as a column to the final matrix
            if tile_tr != tile_tl:        
                tile_tr._read()
                data = np.hstack((data, tile_tr.data))       

            # if there is another tile on the bottom..
            if tile_bl is not None and tile_bl != tile_tl:
                tile_bl._read()        
                if tile_tr != tile_tl: # and if we already added a column..
                    # then join both first horizontally (or else we could end up with a funny but invalid numpy array)
                    tile_br._read()
                    bdata = np.hstack((tile_bl.data, tile_br.data))            
                else: # ok, just one tile so far, now just join one, no need to add the bottomright one
                    bdata = tile_bl.data

                # and now add this matrix bdata as a new row to the final matrix
                data = np.vstack((data, bdata))
            
            # now limit the size of the matrix based on the boundaries
            xres, yres = tile_tl.resolution.values()
            xtl, ytl = tile_tl.boundary['left'], tile_tl.boundary['top']

            # find the index in the matrix
            idx_x1 = int((xmin - xtl) / xres)
            idx_y1 = int((ytl - ymax) / yres)
            idx_x2 = int((xmax - xtl) / xres)
            idx_y2 = int((ytl - ymin) / yres)

            #some optional debugging stuff
            #print(x1, y1, x2, y2)
            #print(tile_tl.shape)
            #print(data.shape)
            
            # select the final matrix
            selection = data[idx_y1:idx_y2, idx_x1:idx_x2]

            # needed for rasterio (what is topleft and the resolution)
            transform = from_origin(xmin, ymax, xres, yres)

            # and code to write the result to a new geotiff file
            filename = str(Path(filepath) / f'{route.code}.tif')
            new_dataset = rasterio.open(filename, 'w', driver='GTiff',
                                    height = selection.shape[0], width = selection.shape[1],
                                    count=1, dtype=str(selection.dtype),
                                    transform=transform)

            new_dataset.write(selection, 1)
            new_dataset.close()            
    i = 1


if __name__=="__main__":
    #update_cpts()
    #update_boreholes()
    #update_soils()
    #upload_routes_from_shapefile("D:/Data/Documents/mlas/waternet/shapefiles/routegeometrie.shp", "waternet")
    #extract_heightdata("waternet", "D:/Data/Documents/mlas/waternet/rasterbestanden/ahn/ahn4_levee")
    fix_boreholecodes()



