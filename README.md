## MLAS

MLAS stands for Modular Levee Assessment System and is work in progress to use as much automation as possible (while keeping things safe!)
in the assessment of levee stability. The system mimics choices of engineers with algorithms and outputs software input or complete
stability results for levees. 

This repo is work in progress with some really nice options that are already implemented;
* automated choice for crest / polder section of a levee (2D approach)
* automated bishop / spencer search grids
* automated phreaticline
* automated search for the normative crosssection in a section of a levee
* automated search for the normative cpt or borehole in a section of a levee

and other options coming soon;
* borehole usage (95% implemented but logic for the cpt / borehole choice needs to be added)
* combining normative crosssection and cpt / borehole to find (and calculate) the normative location
* automated headlines for water in deeper sand layers
* liftvan analysis
* automated calculation with storage in the PostGIS database (immediate output visualisation using GIS)
* and more when I get a new idea (usually while waking up.. yep.. that bad)

**Note** MLAS depends on the MLASEngine code which is private for now. 

## Settings

Sample settings;

```
OWNERS = ["waternet"]
PATH_MLAS = "D:\Data\Documents\mlas"
DIR_SOILINVESTIGATION = "grondonderzoek"
DIR_RASTERDATA = "rasterbestanden"
DIR_CPTS = "sonderingen"
DIR_BOREHOLES = "boringen"
DIR_LOGS = "logs"
DIR_HEIGHTDATA = "ahn/ahn4_levee" # cut out tiles
DIR_RAW_HEIGHTDATA = "ahn/ahn4" # full tiles
DIR_DITCHDATA = "sloten"
DIR_WATERBOTTOM = "waterbodem"
DIR_CROSSSECTIONS = "dwarsprofielen"
```

## Clients

The system can handle multiple clients, add them to ```OWNERS``` in ```settings.py```

## Soilinvestigations

### CPTs
Put all CPTs in folder ```PATH_MLAS\CLIENT\DIR_SOILINVESTIGATION\DIR_CPTS```

Use ```maintenance.py``` function ```update_cpts()``` to update the cpts to the database. 
Errors will be written to ```PATH_MLAS\owner\cptimport.log```

### Boreholes
Put all borehole files in folder ```PATH_MLAS\CLIENT\DIR_SOILINVESTIGATION\DIR_BOREHOLES```

Use ```maintenance.py``` function ```update_boreholes()``` to update the boreholes to the database. 
Errors will be written to ```PATH_MLAS\owner\boreholeimport.log```

**Tip** If you add more CPTs or boreholes simply run the update functions again. 

## Soiltypes and parameters

**Note** The default soils used in automatic CPT conversion methods need to be available in ```mlasengine.objects.soilcollection.py``` in ```DEFAULT_CPT_SOILS```

Use ```maintenance.py``` function ```update_soils``` to update the soils in the database.
This will add all default soils found in ```mlasengine.objects.soilcollection.py``` in ```DEFAULT_CPT_SOILS```.

Every soilcode found in the borehole files will be translated to a shortcode. The relation between the 
initial soilcode and the shortcode is stored in the ```soilcodeconversions``` table from the database. The
database contains a ```soils``` table where **only** shortcodes can be used to find the parameters. 

**Note** you will have to run ```update_soils``` with the ```update``` parameter to True to overwrite existing parameters or
else the existing parameters will be used.

You can retrieve all available soil types using the ```database.get_soils_as_soilcollection(owner)``` option in MLASEngine. 
Note that all soilnames in a borehole will be converted to shortcodes to get the parameters.  

## Typical jobs

### Create crosssections

Create crosssection for a levee based on the available heightdata and (if available) ditch and leveedepth data.

Example;

```
generate_crosssections(owner='waternet', levee_code='A123')
```

from ```generate.py```

Parameters;
* owner: str, name of the owner
* levee_code: str, code for the levee
* [optional] update: bool, if True updates crosssections that are already in the database
* [optional] x_water: int, distance towards the water, default 20m
* [optional] x_polder: int, distance towards the polder, default 50m
* [optional] apply_rdp: bool, apply RDP algorithm to remove obsolete points, default True
* [optional] rdp_epsilon: float, smoothing parameter for RDP algorithm, default 0.1
* [optional] granularity: int, distance between two crosssections, default 10m

![crosssections](https://gitlab.com/breinbaas/mlas/-/raw/master/img/crosssections.png)

### Create normative crosssections

Automatically find the normative crosssection for stretches of levees. The algorithm applies a weight based on the height data of the crosssection and returns a user defined number of crosssections ordered by their relevance. A plot with all crosssections is supplied to make sure the algorithms did the right thing. 

Example;

```
normative_crosssections(
    owner='waternet',
    levee_code='A117',
    limits = [(0,1160),(1160,9999)],
    output_path="D:/_TEMP",
    plot=True
)
```

from ```generate.py```

Parameters;
* owner: str, name of the owner
* levee_code: str, code for the levee
* limits: List[Tuple[int, int]], list with start and end chainage (for example [(0,100), (100, 300)] etc)
* [optional] num_results: int, the number of crosssections to return, defaults to 3
* [optional] output_path: str, if set this will export the crosssections to the given path, defaults to "" (no output)
* [optional] plot_result: bool, plot the result (only works if outputpath is set), defaults to False

![normative crosssections](https://gitlab.com/breinbaas/mlas/-/raw/master/img/normative_crosssections.png)

### Find normative soil investigation

This function uses an algorithm to find the normative soil investigation in a list of CPTs and / or boreholes. 
The soil investigation is weight on the cohesion and friction angle over the given minimum depth. The result
is a list of soil investigations order by the one with the (probable) weakest soil layers.

Example;

```python
db = Database()

cpts, boreholes = [], [] 

cpts.append(db.get_cpt_by_id(15479))
cpts.append(db.get_cpt_by_id(15481))
cpts.append(db.get_cpt_by_id(15482))
boreholes.append(db.get_borehole_by_id(228))
boreholes.append(db.get_borehole_by_id(229))

# find the normative soil investigation in the cpts and boreholes
norm_si = normative_soilinvestigation(owner='waternet', cpts=cpts, boreholes=boreholes)
```

from ```generate.py```

Parameters;
* owner: name of the owner
* cpts: list with the cpts to look at
* boreholes: list with boreholes to look at
* minimum_depth: depth over which to look for the strength parameters, defaults to 3m
* num_results: int, the number of soil investigations to return, defaults to 1

TODO > explain algorithms

**Note** will raise an ValueError if soil investigation is not long enough (< minimum depth)

**Note** this will only work if strength parameters for all given soilcodes have been provided

### Create stix file based on soilinvestigations

**Note** please read the [algorithms](https://gitlab.com/breinbaas/mlas/-/blob/master/ALGORITHMS.md) section to see how 
the implemented algorithms can help you to fill in details for the necessary input.

Generate a 2D (crest and polder) stix file using a crosssection and CPT or borehole information. CPTs can be interpreted using different methods and different layer heights. If the levee and polder waterlevel is added to the function the algorithm will automatically add a phreatic line. The end result is a ready to use Deltares stix file. 

Example;

```
# get a crosssection from the database
db = Database()
crs = db.get_crosssection('waternet', 'A123', 1290)

# load two CPTs (quite random ones actually)
cpt_left = CPT.from_file("D:/Data/Documents/mlas/waternet/grondonderzoek/sonderingen/VAK_B02/B02-631.gef")
cpt_right = CPT.from_file("D:/Data/Documents/mlas/waternet/grondonderzoek/sonderingen/VAK_B02/B02-635.gef")

# generate stix file
stix_from_soilinvestigation(
    owner='waternet',
    crosssection=crs, 
    object_left = cpt_left, 
    object_right = cpt_right,
    fillmaterial_top='clay',
    waterlevel = 0.0,
    waterlevel_polder = -2.5,
    output_path="D:\_TEMP"        
)
```

from ```generate.py```

Parameters;
* crosssection (Crosssection): the crosssection to use
* fillmaterial_top (str): soilcode of the soil to be used as a top material if the top of the CPT / borehole is below the highest point of the crosssection, soilcode needs to be in the soilcollection (in other words, in the soils database table)
* object_left (Union[CPT, Borehole]): the CPT or borehole to use for the left side (water) of the levee
* [optional] object_right (Union[CPT, Borehole]): the CPT or borehole to use for the right side (polder) of the levee, if not set then the left will be used
* [optional] waterlevel (float): waterlevel in the river, defaults to np.nan
* [optional] waterlevel_polder (float): waterlevel in the polder, defaults to np.nan
* [optional] waterlevel_offset (float): minimum distance between maaiveld and phreatic waterline, defaults to 0.1
* [optional] output_path (str): filepath to save the stix file to, defaults to "" (no output)

![stix](https://gitlab.com/breinbaas/mlas/-/raw/master/img/stix.png)

**Note** you can choose a CPT or borehole for ```object_left``` and ```object_right```. It's perfectly fine to mix CPTs with boreholes

**Note** if you only provide the object_left then this will be used for the entire geometry

**Note** the transition between the left and right soillayers is based on l_transition in the crosssection which is automatically determined

**Note** if you want to use the automated phreatic line creation method you will have to fill in the waterlevel, waterlevel_polder and waterlevel_offset parameters or else your input will be ignored.

### Create stix file for download using FastAPI

This code is useful for webservices that create stix files and offer them as a download.

If you use ```stix_from_soilinvestigation``` without setting the output path you will get a io.BytesIO object which can then be passed as a
download using code like;

```
from fastapi.responses import Response

# create buffer
zip_buffer = stix_from_soilinvestigation(
    owner='waternet',
    crosssection=crs, 
    object_left = cpt_left, 
    object_right = cpt_right,
    fillmaterial_top='clay',
    waterlevel = 0.0,
    waterlevel_polder = -2.5,
    output_path="D:\_TEMP"        
)

# return buffer in web response using FastAPI
resp = Response(
    zip_buffer.getvalue(),
    media_type="application/zip",
    headers={"Content-Disposition": f'attachment;filename="output.stix"'},
)
```

**Note** this will only work with MLASEngine since this has a slightly adjusted geolib version.

### Create stix file based on manual soillayers

Use this function to create a stix file from user defined soillayers. 

Todo


### Generate stix files for part of the levee

Use this function to create stix files for a part (or the entire) levee

**Note** the end result of the code will be an automated inward stability assessment, for now
you will get the stix files, auto calculation will be implemented soon.

```python
db = Database()

# get the crosssection you are interested in
crosssections = db.get_crosssections_from_chainages('waternet', 'A117', [2080, 2120, 2240, 2450])    

# add the crest and polder CPTs
cpts_crest, cpts_polder = [], []

# the easiest way to add them is to look them up in QGis which also makes sure
# that you choose the right soil investigations to use for the model. 
# there will be an automatic way some time in the future but for now this
# leads to better quality
cpts_crest.append(db.get_cpt_by_id(15479))
cpts_crest.append(db.get_cpt_by_id(15481))
cpts_crest.append(db.get_cpt_by_id(15482))
cpts_polder.append(db.get_cpt_by_id(15480))
cpts_polder.append(db.get_cpt_by_id(15456))
cpts_polder.append(db.get_cpt_by_id(15483))

# generate the assessment class
stbi = AssessmentLeveeStabilityInward(
    owner = 'waternet',
    cpts_crest = cpts_crest, # your selection of crest CPTs
    cpts_polder = cpts_polder, # your selection of polder CPTs
    crosssections = crosssections, # your selection of crosssections
    fillmaterial_top = 'clay', # fill material if the crosssection is higher than the soil investigation
    pl_setting_areas = [SettingsArea( # see below
        chainage_start= 2020,
        chainage_end = 2500,
        settings = AlgorithmSettingsAddPhreaticLine(
            waterlevel = 0.0,
            waterlevel_polder = -5.0
        )
    )],
    bbf_setting_areas = [SettingsArea( # see below
        chainage_start= 2020,
        chainage_end = 2500,
        settings = AlgorithmSettingsDStabilityBishopBruteForce(
            searcharea_min_width = 10, 
            searcharea_min_height = 10,
            searcharea_resolution = 1,
            tangentarea_zmin = -8,
            tangentarea_min_height = 5,
            tangentarea_resolution = 1    
        )
    )],
    output_path = "D:\_TEMP" # export all data to this path
)

stbi.execute() # start assessment
```

Executing this function will automatically create all the required stix files (and later report the calculated stability..)

![stixfiles](https://gitlab.com/breinbaas/mlas/-/raw/master/img/stixfiles.png)

Parameters;
* owner (str): name of the owner
* crosssections (List[Crosssection]): a list of crosssections that will be assessed
* [optional] cpts_crest (List[CPT]) list of CPTs on the crest of the levee
* [optional] cpts_polder (List[CPT]) list of CPTs on the polder side of the levee
* [optional] boreholes_crest (List[CPT]) list of boreholes on the crest of the levee **not implemented yet!**
* [optional] oreholes_polder (List[CPT]) list of boreholes on the polder side of the levee **not implemented yet!**
* fillmaterial_top (str): soilcode of the soil to be used as a top material if the top of the CPT / borehole is below the highest point of the crosssection, soilcode needs to be in the soilcollection (in other words, in the soils database table)
* [optional] pl_setting_areas (List[SettingsArea]), a list with phreatic line settings (**see settingsarea chapter below this code**)
* [optional] bbf_setting_areas (List[SettingsArea]), a list with phreatic line settings (**see settingsarea chapter below this code**)
* [optional] output_path (str): filepath to save the stix file to, defaults to "" (no output)

**Note** Even though all CPT and Boreholes are optional you will off course need at least one CPT or borehole to make sure that soillayers can be created

#### SettingsArea

A settingsarea is an area where certain defined settings apply. This can be settings for the phreatic line algorithm or other ones. You need to define a start and end chainage and the settings that apply within that region of the levee. So for example;

```python
SettingsArea( 
    chainage_start= 2020,
    chainage_end = 2500,
    settings = AlgorithmSettingsAddPhreaticLine(
        waterlevel = 0.0,
        waterlevel_polder = -5.0
    )
)
```

Will create phreaticline setting with a waterlevel in the levee of 0.0 and in the polder of -5.0. This will only be true for all crosssections within the given chainage limits, in this case for all crosssections between 2020 and 2500 (including the limits).

An assessment can have multiple settingareas to allow different settings for different parts of the levee.

### Create geotechnical profile

Use this function to generate a geotechnical profile using given CPTs and boreholes.

Todo

### Perform height check

Todo
